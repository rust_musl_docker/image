# rust_musl_docker – A well-documented container for building Rust crates for MUSL target. Supports dependencies for OpenSSL and Postgres.

**Note: This project is outdated and abandoned! I'm leaving it online as it has gathered some stars,
and there might be some CI setups or scripts that depend on it, but it is no longer maintained.**

This docker image is primarily meant for building statically Rust crates that use **Diesel** and **Rocket** libraries.
A combination of static linking, native (C) dependencies and crates that use heavily compiler plugins is hard to get to
compile. (1) This Docker image is meant to help with that. Not only it supports Diesel and Rocket (and many other crates)
directly, it also is fully commented to help for possible customisation needs. There exists other similar images too,
but the lack of comments make them unhelpful if they don't happen to contain the exact things you need.

`BaseDockerfile.template` and `RustDockerfile.template` are fully commented! Please read them for details.

New images are automatically built daily and stored in this repository. Here's some useful tags:

```
registry.gitlab.com/rust_musl_docker/image:stable-latest              # An image with the newest stable build (updated continuously)
registry.gitlab.com/rust_musl_docker/image:beta-latest                # The newest beta (updated continuously)
registry.gitlab.com/rust_musl_docker/image:stable-1.30.0              # Rust stable releases with the version number (updated when I have time and energy to do so)
registry.gitlab.com/rust_musl_docker/image:nightly-2018-10-27         # Rust nightly releases with the date (updated continuously)
```

## UPDATES:

* 2022-11-02: Updated base image to OpenSSL 1.1.1s and Postgres to 11.17. Update Rust stable image to 1.64.0.
* 2021-03-29: Updated base image to OpenSSL 1.1.1k. Update Rust stable image to 1.51.0.
* 2021-03-03: Updated base image to OpenSSL 1.1.1j and Postgres to 11.11. Updated Rust stable image to 1.50.0.
* 2020-10-05: Updated base image to OpenSSL 1.1.1h (Postgres stays at 11.7). Updated Rust stable image to 1.46.0.
* 2020-05-10: Updated base image to OpenSSL 1.1.1g (Postgres stays at 11.7).
* 2020-05-10: Updated the Rust stable image to 1.43.1.

## USAGE:

Run this container (you can do that directly but I recommend writing a build script) from your project dir.

Mount the current work dir (`-v $PWD:/workdir`) and Cargo cache dirs (`-v ~/.cargo/git:/root/.cargo/git` &
`-v ~/.cargo/registry:/root/.cargo/registry`) to be able to reuse the cache.

You can customise the build command; in the example below, it builds the release build super-verbosely for musl target.


```
docker run -it --rm \
    -v $PWD:/workdir \
    -v ~/.cargo/git:/root/.cargo/git \
    -v ~/.cargo/registry:/root/.cargo/registry \
    registry.gitlab.com/rust_musl_docker/image:stable-latest \
    cargo build --release -vv --target=x86_64-unknown-linux-musl
```

(1) This has gotten even harder as of late, with Debian Stretch moving to position-independent-code by default and the Rust compiler enabling RELRO. Fortunately, after a lot of trial and error, the build enviroment works.

## Memo for the maintainer:
### How to run Pipelines to publish a new OpenSSL
* `BUILD`: `base`
### How to run Pipelines to publish a new Rust stable version:
* `BUILD`: `manual`
* `RUST_VER`: for example: `1.51.0`
